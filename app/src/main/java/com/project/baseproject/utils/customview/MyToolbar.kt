package com.project.baseproject.utils.customview

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.*
import androidx.constraintlayout.widget.ConstraintLayout
import com.project.baseproject.R
import kotlinx.android.synthetic.main.tool_bar.view.*


class MyToolbar : ConstraintLayout {
    constructor(context: Context?) : super(context) {
        initView(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initView(context)
        setParams(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView(context)
        setParams(attrs)
    }

    private fun initView(context: Context?) {
        LayoutInflater.from(context).inflate(R.layout.tool_bar, this, true)
    }

    private fun setParams(attrs: AttributeSet?) {
        val types = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MyToolbar, 0, 0)
        setIconLeft(types.getResourceId(R.styleable.MyToolbar_left_icon,-1))
        setIconRight(types.getResourceId(R.styleable.MyToolbar_right_icon,-1))
        showLeft(types.getBoolean(R.styleable.MyToolbar_show_left_icon,true))
        showRight(types.getBoolean(R.styleable.MyToolbar_show_right_icon,true))
        setUpTitle(title = types.getString(R.styleable.MyToolbar_title)?:"",
            size = types.getDimension(R.styleable.MyToolbar_title_size,16f),
            color = types.getInteger(R.styleable.MyToolbar_title_color,Color.BLACK))
        types.recycle()
    }

    fun setIconLeft(@DrawableRes res: Int): MyToolbar {
        imv_left.setImageResource(res)
        return this
    }

    fun setIconRight(@DrawableRes res: Int): MyToolbar {
        imv_right.setImageResource(res)
        return this
    }

    fun showLeft(show: Boolean): MyToolbar {
        imv_left.visibility = if (show) View.VISIBLE else View.GONE
        return this
    }

    fun showRight(show: Boolean): MyToolbar {
        imv_right.visibility = if (show) View.VISIBLE else View.GONE
        return this
    }

    fun setOnClickIcon(listener: OnClickListener): MyToolbar {
        imv_left.setOnClickListener(listener)
        imv_right.setOnClickListener(listener)
        return this
    }

    fun setUpTitle(
        title: String,
        @Dimension size: Float,
        @ColorInt color: Int
    ): MyToolbar {
        tv_title.setText(title)
        tv_title.setTextColor(color)
        tv_title.setTextSize(size)
        return this
    }
}