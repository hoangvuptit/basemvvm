package com.project.baseproject.utils

import android.content.SharedPreferences
import androidx.annotation.NonNull
import javax.inject.Inject


class PreferencesHelper @Inject internal constructor(private val preferences: SharedPreferences) {

    fun putStringSet( @NonNull key: String?,  @NonNull value: HashSet<String>?) {
        preferences.edit().putStringSet(key, value).apply()
    }

    fun getStringSet( @NonNull key: String?): MutableSet<String> {
        return preferences.getStringSet(key, HashSet())!!
    }

    fun putString( @NonNull key: String?,  @NonNull value: String?) {
        preferences.edit().putString(key, value).apply()
    }

    fun getString( @NonNull key: String?): String? {
        return preferences.getString(key, "")
    }

    fun putBoolean(@NonNull key: String?, @NonNull value: Boolean) {
        preferences.edit().putBoolean(key, value).apply()
    }

    fun getBoolean(@NonNull key: String?): Boolean {
        return preferences.getBoolean(key, false)
    }

    fun putInt(@NonNull key: String?,@NonNull  value: Int) {
        preferences.edit().putInt(key, value).apply()
    }

    fun getInt(@NonNull key: String?): Int {
        return preferences.getInt(key, -1)
    }

    fun clear() {
        preferences.edit().clear().apply()
    }

}