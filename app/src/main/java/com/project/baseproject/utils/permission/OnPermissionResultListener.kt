package com.project.baseproject.utils.permission

interface OnPermissionResultListener {
    fun onPermissionGranted()

    fun onPermissionDenied(permissions: ArrayList<String>) {}

    fun onPermissionBlocked(permissions: ArrayList<String>)
}