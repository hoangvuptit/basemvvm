package com.project.baseproject.ui.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.NavOptions
import com.project.baseproject.BuildConfig
import com.project.baseproject.R
import com.project.baseproject.factory.ViewModelFactory
import com.project.baseproject.ui.activity.MainActivity
import com.project.baseproject.utils.permission.PermissionManager
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


abstract class BaseFragment : Fragment() {
    var navController: NavController? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    protected inline fun <reified VM : ViewModel>
            injectActivityViewModels(): Lazy<VM> = activityViewModels { viewModelFactory }

    protected inline fun <reified VM : ViewModel>
            injectFragmentViewModels(): Lazy<VM> = viewModels { viewModelFactory }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setUpView()
        setUpData()
        excutedPermission()
    }

    fun loading(show: Boolean) =
        if (show) (activity as MainActivity).showLoading() else (activity as MainActivity).hideLoading()


    fun navigate(id:Int): NavController? {
        navController?.navigate(id)
        return navController
    }

    fun navigate(id: Int, bundle: Bundle): NavController? {
        navController?.navigate(id, bundle)
        return navController
    }

    fun navigate(id: Int,toId:Int) {
        navController
            ?.navigate(
                id,
                null,
                NavOptions.Builder()
                    .setPopUpTo(
                        toId,
                        true
                    ).build()
            )
    }

    fun backPress(){
        navController?.navigateUp()
    }

    fun excutedPermission(){
        permissionManager().enableLogs(true)

        //set method to be executed when permission granted by user.
        permissionManager().executeOnPermissionGranted { run { /*permissionsGranted()*/ } }
        //set method to be executed when permission denied by user.
        permissionManager().executeOnPermissionDenied { run { permissionsDenied() } }
        //set method to be executed when permission blocked by user.
        permissionManager().executeOnPermissionBlocked { run { permissionsBlocked() } }
    }

    fun permissionManager(): PermissionManager {
        return (activity as MainActivity).mPermissionManager
    }

    fun permissionsBlocked() {
        AlertDialog.Builder(context!!)
            .setMessage(R.string.permission_blocked)
            .setPositiveButton(R.string.settings, { dialogInterface, i ->
                val intent = Intent(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
                )
                startActivityForResult(intent, PermissionManager.PERMISSION_SETTINGS_REQUEST)

            })
            .setNegativeButton(android.R.string.cancel, null)
            .setCancelable(false)
            .show()
    }

    fun permissionsDenied() {
        AlertDialog.Builder(context!!)
            .setMessage(R.string.permission_required)
            .setPositiveButton(
                R.string.grant,
                { dialogInterface, i -> permissionManager().checkAndRequestPermissions() })
            .setNegativeButton(android.R.string.cancel, null)
            .setCancelable(false)
            .show()
    }

    abstract fun getLayoutId(): Int
    abstract fun setUpView()
    abstract fun setUpData()
    abstract fun observeViewModel()
}