package com.project.baseproject.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.project.baseproject.data.remote.api.State
import com.project.baseproject.data.local.entity.MultipleResourceEntity
import com.project.baseproject.data.repository.MultipleRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class OneViewModel @Inject constructor(val multipleRepository: MultipleRepository) : BaseViewModel() {

    val multiples: MutableLiveData<List<MultipleResourceEntity>> by lazy {
        MutableLiveData<List<MultipleResourceEntity>>()
    }

    var datas: MutableList<MultipleResourceEntity> = mutableListOf()


    fun getMultiples() {
        viewModelScope.launch(io) {
            multipleRepository.getDatas().collect { data ->
                when (data) {
                    is State.Success -> multiples.postValue(data.data)
                    is State.Loading -> isLoading.postValue(data.show)
                    is State.Error -> multiples.postValue(data.data)
                }
            }
        }
    }

    fun add() {
        val d = MultipleResourceEntity(1, "Mr.Vu", 1996, "OK")
        datas.add(d)
        multiples.postValue(datas)
    }
}