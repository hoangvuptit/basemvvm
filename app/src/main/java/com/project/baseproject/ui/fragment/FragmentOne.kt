package com.project.baseproject.ui.fragment


import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.lifecycle.Observer
import com.project.baseproject.R
import com.project.baseproject.ui.adapter.MultipleAdapter
import com.project.baseproject.ui.viewmodel.OneViewModel
import kotlinx.android.synthetic.main.fragment_fragment_one.*
import kotlinx.android.synthetic.main.tool_bar.*

class FragmentOne : BaseFragment() {
    private val viewModel: OneViewModel by injectActivityViewModels()

    private lateinit var adapter: MultipleAdapter

    override fun setUpData() {
        viewModel.getMultiples()
    }

    override fun observeViewModel() {
        viewModel.multiples.observe(this, Observer {
            adapter.appenData(it)
        })
        viewModel.isLoading.observe(this, Observer {
            loading(it)
        })
    }


    override fun setUpView() {
        imv_left.setOnClickListener { backPress() }
        btn_add.setOnClickListener { viewModel.add() }
        tv_title.setOnClickListener {
            val slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up)
            val slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down)
            if (btn_add.getVisibility() === View.INVISIBLE) {
                btn_add.setVisibility(View.VISIBLE)
                btn_add.startAnimation(slideUp)
            } else {
                btn_add.setVisibility(View.INVISIBLE)
                btn_add.startAnimation(slideDown)
            }
        }
        adapter = MultipleAdapter {
            Toast.makeText(context, it.name, Toast.LENGTH_LONG).show()
        }
        rcv.adapter = adapter
    }

    override fun getLayoutId(): Int = R.layout.fragment_fragment_one
}
