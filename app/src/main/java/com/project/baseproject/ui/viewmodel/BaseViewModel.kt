package com.project.baseproject.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext


open class BaseViewModel :
    ViewModel() {

    var isLoading = MutableLiveData<Boolean>()

    val io: CoroutineContext
        get() = Dispatchers.IO + handlerEx

    val main: CoroutineContext
        get() = Dispatchers.Main + handlerEx

    val default: CoroutineContext
        get() = Dispatchers.Default + handlerEx

    val handlerEx = CoroutineExceptionHandler { _, exception ->
        Log.d("ERROR_MESSAGE", "Message ----> $exception")
    }
}