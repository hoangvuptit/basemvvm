package com.project.baseproject.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.project.baseproject.R
import com.project.baseproject.data.local.entity.MultipleResourceEntity
import kotlinx.android.synthetic.main.item_multiple.view.*


class MultipleAdapter(
        private val action: (MultipleResourceEntity) -> Unit
) : RecyclerView.Adapter<MultipleAdapter.ViewHolder>() {

    private var list:MutableList<MultipleResourceEntity> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
        R.layout.item_multiple,parent,false))

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item: MultipleResourceEntity = list.get(position)
        with(holder.itemView) {
            tv_name.setText(item.name)
            setOnClickListener { action(item) }
        }
    }

    fun appenData(datas:List<MultipleResourceEntity>){
        list.clear()
        list.addAll(datas)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}