package com.project.baseproject.data.repository

import com.project.baseproject.data.local.dao.MultipleDAO
import com.project.baseproject.data.remote.model.ArrayResponse
import com.project.baseproject.data.remote.model.MultipleResource
import com.project.baseproject.data.remote.api.Output
import com.project.baseproject.data.remote.api.State
import com.project.baseproject.data.local.entity.MultipleResourceEntity
import com.project.baseproject.data.remote.api.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MultipleRepository @Inject constructor(val dao: MultipleDAO, val apiInterface: ApiInterface) :
    BaseRepository() {

    suspend fun getDatas(): Flow<State<out List<MultipleResourceEntity>>> {
        return object :
            NetworkBoundResource<List<MultipleResourceEntity>, ArrayResponse<MultipleResource>>() {
            override suspend fun saveNetworkResult(item: ArrayResponse<MultipleResource>) {
                dao.insert(item.datas.map {
                    MultipleResourceEntity(
                        id = it.id,
                        name = it.name,
                        year = it.year,
                        pantoneValue = it.pantoneValue
                    )
                })
            }

            override fun shouldFetch(data: List<MultipleResourceEntity>?): Boolean = true

            override fun loadFromDb(): Flow<List<MultipleResourceEntity>> = dao.getAll()

            override suspend fun fetchFromNetwork(): Output<ArrayResponse<MultipleResource>> {
                return callApi(
                    call = {
                        apiInterface.getListResources()
                    }
                )
            }
        }.asFlow()
    }

    suspend fun getDatas1(): Flow<State<out ArrayResponse<MultipleResource>>> {
        return flowApi {
            callApi {
                apiInterface.getListResources()
            }
        }
    }
}