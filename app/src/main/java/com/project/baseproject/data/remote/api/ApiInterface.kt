package com.project.baseproject.data.remote.api

import com.project.baseproject.data.remote.model.ArrayResponse
import com.project.baseproject.data.remote.model.MultipleResource
import retrofit2.Response
import retrofit2.http.GET


interface ApiInterface {
    @GET("/api/unknown")
    suspend fun getListResources(): Response<ArrayResponse<MultipleResource>>
}