package com.project.baseproject.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.project.baseproject.data.local.entity.MultipleResourceEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface MultipleDAO {
    @Query("SELECT * from multiple")
    fun getAll(): Flow<List<MultipleResourceEntity>>

    @Insert(onConflict = REPLACE)
    fun insert(multipleResourceEntitys: List<MultipleResourceEntity>)

    @Query("DELETE from multiple")
    fun deleteAll()
}