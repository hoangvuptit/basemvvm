package com.project.baseproject.data.remote.interceptor

import android.content.Context
import android.content.SharedPreferences
import com.project.baseproject.utils.PreferencesHelper
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException


class ReceivedCookiesInterceptor(val sharedPreferences: SharedPreferences) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalResponse: Response = chain.proceed(chain.request())
        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            val cookies: HashSet<String> = HashSet()
            for (header in originalResponse.headers("Set-Cookie")) {
                cookies.add(header)
            }
            PreferencesHelper(sharedPreferences).putStringSet("cookies",cookies)
        }
        return originalResponse
    }
}