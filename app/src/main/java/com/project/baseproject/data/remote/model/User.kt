package com.project.baseproject.data.remote.model

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("job") val job: String,
    @SerializedName("createdAt") val createdAt: String
)