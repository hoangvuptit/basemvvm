package com.project.baseproject.data.repository

import android.util.Log
import com.project.baseproject.data.remote.api.Output
import com.project.baseproject.data.remote.api.EmptyException
import com.project.baseproject.data.remote.api.ErrorException
import com.project.baseproject.data.remote.api.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import retrofit2.Response

open class BaseRepository {
    suspend fun <T : Any> callApi(call: suspend () -> Response<T>): Output<T> {
        val response = withContext(Dispatchers.IO) {
            call.invoke()
        }
        return if (response.isSuccessful) {
            val body = response.body()
            if (body == null || response.code() == 204) {
                Output.Empty(EmptyException("No data"))
            } else {
                Output.Success(body)
            }
        } else {
            Log.d("BASE_REPOSITORY:", response.errorBody()?.string())
            val msg = response.errorBody()?.string()
            val errorMsg = if (msg.isNullOrEmpty()) {
                response.message()
            } else {
                msg
            }
            Output.Error(ErrorException(errorMsg))
        }
    }
}

suspend fun <T> flowApi(call: suspend () -> Output<T>): Flow<State<out T>> {
    return flow {
        val output = call.invoke()
        when (output) {
            is Output.Success -> {
                emit(State.Success(output.data))
            }
            is Output.Empty -> emit(State.Error("Data empty", null))
            is Output.Error -> emit(State.Error(output.exception.message.toString(), null))
        }
    }
        .onStart { emit(State.Loading(true)) }
        .catch { emit(State.Error(it.message.toString(), null)) }
        .onCompletion { emit(State.Loading(false)) }
}