package com.project.baseproject.data.remote.model

import com.google.gson.annotations.SerializedName
data class MultipleResource(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("year") val year: Int,
    @SerializedName("pantone_value") val pantoneValue: String
)