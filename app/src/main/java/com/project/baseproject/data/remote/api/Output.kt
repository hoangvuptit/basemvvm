package com.project.baseproject.data.remote.api

sealed class Output<out T>{
    data class Success<T>(val data : T) : Output<T>()
    data class Error(val exception: Exception)  : Output<Nothing>()
    data class Empty(val exception: Exception)  : Output<Nothing>()
}