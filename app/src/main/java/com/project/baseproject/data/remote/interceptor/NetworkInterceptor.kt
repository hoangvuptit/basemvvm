package com.project.baseproject.data.remote.interceptor

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

private const val CACHE_CONTROL = "Cache-Control"
private const val TIME_CACHE_ONLINE = "public, max-age = 60" // 1 minute

private const val TIME_CACHE_OFFLINE = "public, only-if-cached, max-stale = 86400" //1 day

class NetworkInterceptor(val context: Context): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var builder = chain.request().newBuilder()
//        if (isNetworkAvailable(context)) {
//            builder.header(CACHE_CONTROL, TIME_CACHE_ONLINE)
//                .build()
//        } else {
//            builder.header(CACHE_CONTROL, TIME_CACHE_OFFLINE)
//                .build()
//        }
        return chain.proceed(builder.build())
    }
}