package com.project.baseproject.data.remote.api

import java.lang.Exception

data class EmptyException (override val message:String): Exception(message)

data class ErrorException(override val message:String): Exception(message)
