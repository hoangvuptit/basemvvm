package com.project.baseproject.data.remote.interceptor

import android.content.SharedPreferences
import android.util.Log
import com.project.baseproject.utils.PreferencesHelper
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException


class AddCookiesInterceptor(val sharedPreferences: SharedPreferences) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder: Request.Builder = chain.request().newBuilder()
        val preferences = PreferencesHelper(sharedPreferences).getStringSet("cookies")
            for (cookie in preferences) {
                builder.addHeader("Cookie", cookie)
                Log.v(
                    "OkHttp",
                    "Adding Header: $cookie"
                ) // This is done so I know which headers are being added; this interceptor is used after the normal logging of OkHttp
            }
        return chain.proceed(builder.build())
    }
}