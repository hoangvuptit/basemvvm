package com.project.baseproject.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.project.baseproject.data.local.dao.MultipleDAO
import com.project.baseproject.data.local.entity.MultipleResourceEntity


@Database(entities = arrayOf(MultipleResourceEntity::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun multipleDAO(): MultipleDAO
}