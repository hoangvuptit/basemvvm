package com.project.baseproject.data.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import com.project.baseproject.data.remote.api.Output
import com.project.baseproject.data.remote.api.State
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@FlowPreview
@ExperimentalCoroutinesApi
abstract class NetworkBoundResource<ResultType, RequestType> {

    fun asFlow() = flow {

        /*Show loading*/
        emit(State.Loading(true))

        /*Get data from DB*/
        val dbValue = loadFromDb().first()

        /**
         * if true -> fetch data from server
         * else -> get data from DB
         */

        if (shouldFetch(dbValue)) {
            try {
                val apiResponse = fetchFromNetwork()
                when (apiResponse) {
                    is Output.Success -> {
                        val mData = success(apiResponse)
                        saveNetworkResult(mData.data)
                        emit(State.Loading(false))
                        emitAll(loadFromDb().map { result ->
                            State.Success(result)
                        })
                    }
                    is Output.Error -> {
                        emit(State.Error(apiResponse.exception.message.toString(), dbValue))
                    }
                    is Output.Empty -> {
                        emit(State.Error("Data is empty!", dbValue))
                    }
                }
            } catch (e: Exception) {
                emit(State.Error(e.message.toString(), dbValue))
            } finally {
                emit(State.Loading(false))
            }
        } else {
            emit(State.Loading(false))
            emitAll(loadFromDb().map { result ->
                State.Success(result)
            })
        }
    }

    @MainThread
    protected open fun onFetchFailed() {
        // Implement in sub-classes to handle errors
    }

    @WorkerThread
    protected open fun success(response: Output.Success<RequestType>) = response

    @WorkerThread
    protected abstract suspend fun saveNetworkResult(item: RequestType)

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract fun loadFromDb(): Flow<ResultType>

    @MainThread
    protected abstract suspend fun fetchFromNetwork(): Output<RequestType>
}