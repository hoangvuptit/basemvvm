package com.project.baseproject.data.remote.interceptor


import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException


class CloseStreamInterceptor() : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder: Request.Builder = chain.request().newBuilder()
        builder.addHeader("Connection", "close")
        return chain.proceed(builder.build())
    }
}