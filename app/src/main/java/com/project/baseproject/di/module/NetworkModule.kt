package com.project.baseproject.di.module

import android.app.Application
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.project.baseproject.BuildConfig
import com.project.baseproject.data.local.dao.MultipleDAO
import com.project.baseproject.data.remote.api.ApiInterface
import com.project.baseproject.data.remote.interceptor.AddCookiesInterceptor
import com.project.baseproject.data.remote.interceptor.CloseStreamInterceptor
import com.project.baseproject.data.remote.interceptor.NetworkInterceptor
import com.project.baseproject.data.remote.interceptor.ReceivedCookiesInterceptor
import com.project.baseproject.data.repository.BaseRepository
import com.project.baseproject.data.repository.MultipleRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provide(): Gson {
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(
        sharedPreferences: SharedPreferences,
        networkConnection: NetworkInterceptor
    ): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        val addCookiesInterceptor = AddCookiesInterceptor(sharedPreferences)
        val receivedCookiesInterceptor = ReceivedCookiesInterceptor(sharedPreferences)
        val closeStream = CloseStreamInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient()
            .newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(loggingInterceptor)
//            .addInterceptor(networkConnection)
//            .addInterceptor(addCookiesInterceptor)
//            .addInterceptor(receivedCookiesInterceptor)
//            .addInterceptor(closeStream)
            .build()
    }

    @Provides
    @Singleton
    fun provideNetWorkConnection(context: Application): NetworkInterceptor {
        return NetworkInterceptor(context)
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    @Provides
    @Singleton
    fun provideApiInterface(retrofit: Retrofit): ApiInterface {
        return retrofit.create(ApiInterface::class.java)
    }
}