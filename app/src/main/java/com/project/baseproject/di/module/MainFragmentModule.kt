package com.project.baseproject.di.module

import com.project.baseproject.di.annotaions.FragmentScope
import com.project.baseproject.ui.fragment.FragmentOne
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.coroutines.InternalCoroutinesApi

@Module
abstract class MainFragmentModule {
    @ContributesAndroidInjector
    @FragmentScope
    abstract fun provideOneFragment(): FragmentOne

}