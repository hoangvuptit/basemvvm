package com.project.baseproject.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.project.baseproject.di.annotaions.ViewModelKey
import com.project.baseproject.factory.ViewModelFactory
import com.project.baseproject.ui.viewmodel.OneViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.InternalCoroutinesApi


@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(OneViewModel::class)
    internal abstract fun oneViewModel(viewModel: OneViewModel): ViewModel

}