package com.project.baseproject.di.module

import com.project.baseproject.di.annotaions.ActivityScope
import com.project.baseproject.ui.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [MainFragmentModule::class])
    abstract fun bindMainActivity(): MainActivity
}