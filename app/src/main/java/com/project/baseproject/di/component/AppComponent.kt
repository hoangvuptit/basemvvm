package com.project.baseproject.di.component

import android.app.Application
import android.content.Context
import com.project.baseproject.MyApplication
import com.project.baseproject.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = arrayOf(
        AndroidSupportInjectionModule::class,
        AndroidInjectionModule::class,
        ViewModelModule::class,
        ViewModelFactoryModule::class,
        ActivityModule::class,
        AppModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        LocalModule::class
    )
)
@Singleton
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build():AppComponent
    }

    fun inject(myApplication: MyApplication)
}