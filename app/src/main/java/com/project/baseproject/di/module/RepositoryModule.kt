package com.project.baseproject.di.module

import com.project.baseproject.data.local.dao.MultipleDAO
import com.project.baseproject.data.remote.api.ApiInterface
import com.project.baseproject.data.repository.BaseRepository
import com.project.baseproject.data.repository.MultipleRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideRepository(dao: MultipleDAO, apiInterface: ApiInterface): MultipleRepository {
        return MultipleRepository(dao, apiInterface)
    }
}