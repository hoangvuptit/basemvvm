package com.project.baseproject.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.NonNull
import androidx.room.Room
import com.project.baseproject.data.local.AppDatabase
import com.project.baseproject.data.local.dao.MultipleDAO
import com.project.baseproject.utils.PreferencesHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
const val PRE_NAME = "SHARED"
@Module
class LocalModule {
    @Provides
    @Singleton
    fun provideDatabase(@NonNull application: Application): AppDatabase {
        return Room
            .databaseBuilder(application, AppDatabase::class.java, "database")
            .allowMainThreadQueries()
            .build()
    }

    @Provides
    @Singleton
    fun provideMultipleDAO(db:AppDatabase):MultipleDAO{
        return db.multipleDAO()
    }

    @Provides
    @Singleton
    fun provideSharedPrefs(application: Application): SharedPreferences {
        return application.getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideSharedPrefsHelper(sharedPreferences: SharedPreferences): PreferencesHelper {
        return PreferencesHelper(sharedPreferences)
    }
}